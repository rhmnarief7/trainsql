CREATE TABLE postion(
  id_postion int(2) NOT NULL,
  postion varchar (50)
);
ALTER TABLE postion
    ADD PRIMARY KEY (`id_postion`);

ALTER TABLE employee
    ADD id_postion int(2),
    ADD KEY `id_postion` (`id_postion`);

INSERT INTO postion (`id_postion`, `postion`) VALUES
(1 , 'Direktur'),
(2 , 'Manajer'),
(3 , 'Staff');

UPDATE employee SET id_postion = '1' WHERE employee.id = 1;
UPDATE employee SET id_postion = '2' WHERE employee.id = 2 OR employee.id = 3;
UPDATE employee SET id_postion = '3' WHERE employee.id > 3 ;