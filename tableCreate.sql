CREATE DATABASE javan;
CREATE TABLE employee(
   id int(20),
   nama varchar(50),
   atasan_id int(10),
   company_id int(10)
);

CREATE TABLE company(
   company_id int(20),
   nama varchar(50),
   alamat text
);

ALTER TABLE company
    ADD PRIMARY KEY (`company_id`);

ALTER TABLE `employee`
    ADD PRIMARY KEY (`id`),
    ADD KEY `company_id` (`company_id`) ;

