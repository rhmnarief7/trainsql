CREATE DATABASE company;

-- Create Table Employee
CREATE TABLE employee (
    id int(11) NOT NULL,
    name varchar(255) NOT NULL,
    gender varchar(2),
    status varchar(10),
    borndate date NOT NULL,
    joindate date NOT NULL,
    id_departement int(2),
);
-- Create Table Department
CREATE TABLE department(
  id_departement int(2) NOT NULL,
  departement varchar (50)
);
--index Table Department
ALTER TABLE `department`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_departement` (`id_departement`);

-- INSERT TABLE FOR DEPARTEMENT
INSERT INTO `department` (`id_departement`, `departement` ) VALUES
(1, 'manajemen'),
(2, 'Pengembangan Bisnis'),
(3, 'Teknisi'),
(4, 'Analis');

-- INSERT TABLE FOR EMPLOYEE

INSERT INTO `employee` (`id`, `name`, `gender`, `status`, `borndate`, `joindate`, `id_departement`) VALUES
(1, 'Rizki Saputra', 'L', 'Menikah', '1980-11-10', '2011-01-01', '1' ),
(2, 'Farhan Reza', 'L', 'Belum', '1989-10-01', '2011-01-01', '1' ),
(3, 'Riyando Adi', 'L', 'Menikah', '1977-01-25', '2011-01-01', '1' ),
(4, 'Diego Manuel', 'L', 'Menikah', '1983-02-22', '2012-09-04', '2' ),
(5, 'Satya Laksana', 'L', 'Menikah', '1981-01-12', '2011-03-19', '2' ),
(6, 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-06-15', '2' ),
(7, 'Putri Persada', 'P', 'Menikah', '1988-01-30', '2013-04-14', '2' ),
(8, 'Alma Safira', 'P', 'Menikah', '1991-05-18', '2013-09-28', '3' ),
(9, 'Haqi Hafiz', 'L', 'Belum', '1995-09-19', '2015-03-09', '3' ),
(10, 'Abi Isyawara', 'L', 'Belum', '1991-06-03', '2012-01-22', '3' ),
(11, 'Maman Kresna', 'L', 'Belum', '1993-08-21', '2012-09-15', '3' ),
(12, 'Nadia Aulia', 'P', 'Belum', '1989-10-07', '2012-05-07', '4' ),
(13, 'Mutiara Rezki', 'P', 'Menikah', '1988-03-23', '2013-05-21', '4' ),
(14, 'Dani Setiawan', 'L', 'Belum', '1986-02-11', '2013-11-30', '4' ),
(15, 'Budi Putra', 'L', 'Belum', '1995-10-23', '2015-12-03', '4' );


