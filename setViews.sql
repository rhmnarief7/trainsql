# CEO
SELECT nama as CEO
FROM employee
WHERE atasan_id IS NULL;

# STAFF
SELECT nama as staff FROM employee WHERE atasan_id > 4 ;

# DIREKTUR
SELECT nama as direktur FROM  employee WHERE atasan_id = 1 AND atasan_id IS NOT NULL;

# MANAGER
SELECT nama as manager  FROM employee WHERE  atasan_id BETWEEN 2 AND 4 ;

# BAWAHAN PAK BUDI
SELECT COUNT(id) AS bawahan from employee WHERE employee.nama != 'Pak Budi';

# BAWAHAN BU SINTA
SELECT COUNT(id) AS bawahan FROM employee WHERE  employee.nama != 'Bu Sinta' AND atasan_id IS NOT NULL ;

